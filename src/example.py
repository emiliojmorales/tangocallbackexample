# This file is part of the TangoCallbackExample distribution
# (https://gitlab.com/emiliojmorales/tangocallbackexample).
# Copyright (c) 2024 Emilio Jose Morales Alejandre.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

__version__ = "0.0.1"

import time
import traceback

import tango

DEVICE = "sys/tg_test/1"
ATTRIBUTE = "ampli"
EVENT_TYPE = tango.EventType.CHANGE_EVENT


class myCallback(object):

    def __init__(self) -> None:
        super(myCallback, self).__init__()

    def push_event(self, evt):
        try:
            print("Event received!!")
            print(f"Attribure:{evt.attr_name}")
            print(f"Event value:{evt.attr_value.value}")

        except Exception as e:
            print(f"Exception in push_event: {e}")
            print(traceback.format_exc())

def _subscriveDP(callback, evt_mode):
    try:
        dp = tango.DeviceProxy(DEVICE)
        dp.subscribe_event(ATTRIBUTE, evt_mode, callback)
        return dp

    except Exception as e:
        print(f"Problems in _subscriveDP:{e}")
        print(traceback.format_exc())

def _subscriveAP(callback, evt_mode):
    try:
        ap = tango.AttributeProxy(f"{DEVICE}/{ATTRIBUTE}")
        ap.subscribe_event(evt_mode, callback)
        return ap

    except Exception as e:
        print(f"Problems in _subscriveAP:{e}")
        print(traceback.format_exc())

if __name__ == "__main__":

    # Example using DeviceProxy:
    cb_dp = myCallback()
    deviceProxy = _subscriveDP(cb_dp, EVENT_TYPE)

    #Example using AttributeProxy:
    # cb_ap = myCallback()
    # attributeProxy = _subscriveAP(cb_ap, EVENT_TYPE)

    new_proxy = tango.AttributeProxy(f"{DEVICE}/{ATTRIBUTE}")

    for i in range(10):
        # Check that events arrive:
        new_proxy.write(i)
        time.sleep(1)